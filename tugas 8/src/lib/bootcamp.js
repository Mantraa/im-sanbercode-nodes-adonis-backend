import Employee from "./employee";
import fs from "fs";
import "core-js/stable";
const path = "data.json";

class Bootcamp {
    static register(input) {
      let [name, password, role ] = input.split(",");
      fs.readFile(path,(err, data)=>{
        if(err){
          console.log(err);
        }
        let existingData = JSON.parse(data);
        let employee = new Employee(name, password, role);
        existingData.push(employee);
        fs.writeFile(path, JSON.stringify(existingData), (err) =>{
          if(err){
            console.log(err)
          } else{
            console.log("berhasil register");
          }
        } );
      });
    }
}
export default Bootcamp;