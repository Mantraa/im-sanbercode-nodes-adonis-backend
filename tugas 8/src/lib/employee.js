class Employee{
    constructor(name, password, role){
      this._name = name;
      this._password = password;
      this._role =role ;
      this._islogin = false ;

    }

    get name(){
      return this._name;
    }

    set name(newName){
      this._name = newName;
    }


    get role() {
      return this._role;
    }

    set role(newRole){
      this._role = newRole;
    }
    
    get password() {
      return this._password;
    }

    set password(newPassword){
      this._password = newPassword;
    }


    get islogin(){
      return this._islogin;
    }

    set islogin(status){
      this._name = status;
    }


  }


 export default Employee;