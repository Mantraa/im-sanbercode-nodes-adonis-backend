"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Employee = /*#__PURE__*/function () {
  function Employee(name, password, role) {
    _classCallCheck(this, Employee);

    this._name = name;
    this._password = password;
    this._role = role;
    this._islogin = false;
  }

  _createClass(Employee, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(newName) {
      this._name = newName;
    }
  }, {
    key: "role",
    get: function get() {
      return this._role;
    },
    set: function set(newRole) {
      this._role = newRole;
    }
  }, {
    key: "password",
    get: function get() {
      return this._password;
    },
    set: function set(newPassword) {
      this._password = newPassword;
    }
  }, {
    key: "islogin",
    get: function get() {
      return this._islogin;
    },
    set: function set(status) {
      this._name = status;
    }
  }]);

  return Employee;
}();

var _default = Employee;
exports["default"] = _default;