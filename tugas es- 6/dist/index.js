"use strict";

var _soal = require("./lib/soal");

var myArgs = process.argv.slice(2);
var command = myArgs[0];

switch (command) {
  case 'sapa':
    var nama = myArgs[1];
    console.log((0, _soal.sapa)(nama));
    break;

  default:
    break;
}